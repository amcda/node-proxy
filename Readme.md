# How To install this repository

We can install the master branch using the following commands:

    git clone https://chathura_alahakoon@bitbucket.org/amcda/node-proxy.git

Then navigate the source folder:

    cd node-proxy

Install the modules as usual using npm:

    npm install


# To Run the Development Backend Server

We can start the Node REST API proxy server application backend with the following command:

    node app.js

