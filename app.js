'use strict';

var express = require('express')
var bodyParser = require('body-parser');
var requestify = require('requestify');
var request = require('request');

const app = express()

// Server configuration & constants
var PORT = process.env.PORT || 3000;
var BASE_URL = 'http://interview-test-1.s3amzwagac.us-east-1.elasticbeanstalk.com';
var GET_SKILLS_URL = 'http://nlu-preclaim-travelers-sandbox.rozie.ai/data/apps/9/skills?api_key=AIzaSyD6A8goJfQOkgHBNAF9Bd-Wvn3mYM9y55Y&fields=label,skill_id&static=0';
var GET_CONCEPTS_URL = 'http://nlu-preclaim-travelers-sandbox.rozie.ai/concept-graph/apps/1/concepts?api_key=AIzaSyD6A8goJfQOkgHBNAF9Bd-Wvn3mYM9y55Y&fields=concept';
var AUTH_TOKEN = '?client_secret=50af6c74-9f88-11e7-90d5-0a0a237c2fe8';
var CLIENT_ID = '&client_id=AC';
var API_OPTIONS = {
    headers: { 'Content-Type': 'application/json' }
};

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Get: All APIs
app.get('/api/get/all', function (req, res) {
    request(BASE_URL + req.url + AUTH_TOKEN + CLIENT_ID, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log('Get all APIs request success');
            res.send(body);
        } else {
            console.log('Error while getting all APIs');
            res.send(error);
        }
    });
});

// Get: All APIS by api name 
// Param:api_name
app.get('/api/get/', function (req, res) {

    if (!req.query || !req.query.api_name) return notFound(res);

    request(BASE_URL + '/api/get' + AUTH_TOKEN + CLIENT_ID + '&api_name=' + req.query.api_name, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log('Get API request success for api_name:', req.query.api_name);
            res.send(body);
        } else {
            console.log('Error while getting API for api_name:', req.query.api_name);
            res.send(error);
        }
    });
});

// Get: All skills
app.get('/skill/get', function (req, res) {
    request(GET_SKILLS_URL, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log('Skills successfully loaded');
            res.send(body);
        } else {
            console.log('Error while getting  skill');
            res.send(error);
        }
    });
});

// Get: All Concepts
app.get('/concept/get', function (req, res) {
    request(GET_CONCEPTS_URL, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            console.log('Concepts successfully loaded');
            res.send(body);
        } else {
            console.log('Error while get concepts');
            res.send(error);
        }
    });
});

// POST: Register new API
app.post('/api/register', function (req, res) {
    var URL = BASE_URL + req.url + AUTH_TOKEN;
    var record = req.body;
    requestify
        .post(URL, record, API_OPTIONS)
        .then(function (response) {
            res.json(response);

        })
        .catch(function (err) {
            res.json(err);
            console.log('Error while registering new API');
        });
});

// POST: Map skill with APIs
app.post('/skill/create', function (req, res) {
    var URL = BASE_URL + req.url + AUTH_TOKEN;
    var record = req.body;
    requestify
        .post(URL, record, API_OPTIONS)
        .then(function (response) {
            res.json(response);

        })
        .catch(function (err) {
            res.json(err);
            console.log('Error while skill creating with API');
        });
});

// Error handling fuctions
function notFound(res) {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('404 : Query parameter not found');
}

app.listen(PORT, function () {
    console.log('Node proxy service listening on port %s.', PORT);
});
